#! python 3
#HealthyEatingJJ.py
#Produces a table of multiple users and their 5 a day fruit and veg
#John Johnson
#200502


#initialise variables
prompt=['First ','Second','Third ','Fourth','Fifth ']
count=0
myfav=[]

#get user name and store in myfav
while True:# while loop iterates until no new user
    print()
    user=input("User name? \t")
    print()
    myfav.append(user)
    print('OK,', user,', ' 'Please enter each of your five portions of fruit or veg.   ' )

    
#get five fruits from user and store in myfav
    for n in range(5):
        print(prompt[n],"item ",end=" \t")    
        myfav.append(input())
    count=count+1# This is used to allow for another user
    print()
    #while loop is tested for next user or not.
    if input("Next user?, 'yes' to continue \t")!= 'yes':
        break
print()

   
#print table showing users and their items of fruit and veg
print(" Name", end="\t\t")    
for n in range(5):    
    print(prompt[n], end="\t\t")#Headings for table
    if n==5:
        print("\n")
for m in range (6*(count)):
    if m % 6 == 0 or m==0:# new line after 6items taken from list(user name plus five fruit and veg
        print("\n", myfav[m],end="\t\t")
    else:
        print(myfav[m], end="\t\t")