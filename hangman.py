# hangman.py
# Team version.
# 28/5/2020

# Hangman game.

# Import modules
import random

# Game variables
wrong_guesses=10
word_len_min=4
word_len_max=13
letters_tried=[]    # All the letters that the user has entered

def get_word():
    #Function to get secret word
    #Enable 1 of the 2 lines below depending on your operating system.
    #filename = "./nouns.txt"   #Linux / Mac
    filename = 'nouns.txt'      #Windows
    with open(filename,'r') as reader:
        whole_text = reader.read()
    words_list = whole_text.split()
    index  = random.randint(0, len(words_list)-1)
    return words_list[index]

def user_entry():
    # Get a single character from the user. Convert it to lower case
    flag_letterentry=True
    while flag_letterentry:
        char=input('Enter a letter:')
        char=char.lower()
        if len(char)!=1:
            print('Please enter just 1 letter')
        elif 96 < ord(char) < 123:  # Check character between 'a' and 'z'
            flag_letterentry=False  # Entry valid so set flag to exit loop
        else:
            print('Please enter a letter between "a" and "z"')
    return char
            

#----------------------------------------------------------------
print(get_word())

#print(user_entry())
